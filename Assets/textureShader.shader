﻿Shader "Custom/textureShader" {
	Properties{
		_Color("Color Tint", Color) = (1,1,1,1)
		_MainTex("Diffuse Texture", 2D) = "white" {}
		
	}
		SubShader{
			Pass{
			Tags{ "LightMode" = "ForwardBase" }
			CGPROGRAM
			#pragma vertex vertexFunction
			#pragma fragment fragmentFunction

			#include "UnityCG.cginc"

			//user defined variables
			uniform float4 _Color;
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float4 _LightColor0;

			//unity defined variables

			//input struct
			struct inputStruct
			{
				float4 vertexPos : POSITION;
				float4 textureCoord : TEXCOORD0;
				float3 normal : NORMAL;
			};

			//output struct
			struct outputStruct
			{
				float4 pixelPos : SV_POSITION;
				float4 tex : TEXCOORD0;
				half4 col : COLOR;
			};

			//vertex program
			outputStruct vertexFunction(inputStruct input)
			{
				outputStruct toReturn;

				float4 normal = float4(input.normal, 0.0);

				float3 normalNormal = normalize(mul(normal, unity_WorldToObject));
				float3 normalLight = normalize(_WorldSpaceLightPos0);

				float3 dotProd = dot(normalNormal, normalLight);

				float3 diffuseDirection = _LightColor0.rgb * _Color.rgb * max(0.0, dotProd);

				toReturn.pixelPos = mul(UNITY_MATRIX_MVP, input.vertexPos);

				//assigning the info from inputStruct that will pass to the fragment program 
				//for it to use
				toReturn.tex = input.textureCoord;
				toReturn.col = float4(diffuseDirection, 1.0);
				return toReturn;
			}

			//fragment program
			float4 fragmentFunction(outputStruct input) : Color
			{
				float4 tex = tex2D(_MainTex, _MainTex_ST.zw * input.tex.xy + _MainTex_ST.xy);

				return tex * input.col;
			}

			
				ENDCG
		}
		
	}
		Fallback "Diffuse"
}


