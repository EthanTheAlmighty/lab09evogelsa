﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/inClassShader"
{
	Properties
	{
		//interface -- communictaes between unity and the shader


		//create color picker
		//Handle (Name, Type) = Default Value
		_Color("Main Color -- I can change this!", Color) = (1,1,1,1)


	}
		Subshader
	{
		Pass
	{
		Tags{ "LightMode" = "ForwardBase" }


		//notates the start of shader CG code
		CGPROGRAM


		//declare pragmas
#pragma vertex vertFunc
#pragma fragment fragFunc


#include "UnityCG.cginc"


		//store properties as variables - (translate the property to something CG recognizes)
		uniform float4 _Color;
		uniform float4 _LightColor0;



	//uses semantics to initialize values to input the vertex function
	struct vertexInput
	{
		float4 pos : POSITION;
		float3 normal : NORMAL;
	};


	//is created and returned in the vertex function, and passed to the fragment function
	struct vertexOutput
	{
		float4 pos : SV_POSITION;
		float4 col : COLOR;
	};

	//functions
	vertexOutput vertFunc(vertexInput input)
	{
		//data type struct
		vertexOutput output;

		//change float3 to float4
		float4 normal = float4(input.normal, 0.0);

		//multiplying normal matrix by world object space matrix
		//normalize
		float3 normalNormal = normalize(mul(normal, unity_WorldToObject));

		float3 normalLight = normalize(_WorldSpaceLightPos0);
		float3 dotProd = dot(normalNormal, normalLight);
		float3 diffuseDirection = _LightColor0.rgb * _Color.rgb * max(0.0, dotProd);

		//apply color to model
		output.col = float4(diffuseDirection, 1.0);
		output.pos = mul(UNITY_MATRIX_MVP, input.pos);

		return output;
	}


	half4 fragFunc(vertexOutput output) : Color
	{
		return output.col;
	}


		//notates end of cg code
		ENDCG
	}
	}
		Fallback "Diffuse"
}